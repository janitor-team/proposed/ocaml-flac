ocaml-flac (0.3.0-1) unstable; urgency=medium

  * Fix d/watch for new uptream release source
  * New upstream version 0.3.0
    - Fixes FTBFS (Closes: #1001113)
  * Drop patches fixed upstream
  * Use dune as upstream build system
  * Fix linking against libflac
  * Drop copyright references to old files
  * Depend on required shlibs
  * Bump Standards-Version to 4.6.0 (no change)

 -- Kyle Robbertze <paddatrapper@debian.org>  Mon, 06 Dec 2021 10:36:59 +0200

ocaml-flac (0.2.0.1~really0.1.7-1) unstable; urgency=medium

  * Reupload 0.1.7 so that Liquidsoap builds correctly. 0.2.0
    will only be supported in liquidsoap 2.0 and liquidsoap is
    the main r-dep for ocaml-flac.

 -- Kyle Robbertze <paddatrapper@debian.org>  Mon, 21 Dec 2020 08:56:02 +0200

ocaml-flac (0.2.0-1) unstable; urgency=low

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Kyle Robbertze ]
  * New upstream version 0.2.0

 -- Kyle Robbertze <paddatrapper@debian.org>  Sun, 18 Oct 2020 22:36:40 +0200

ocaml-flac (0.1.7-1) unstable; urgency=medium

  * Team upload
  * New upstream release
  * Update Homepage and debian/watch
  * Remove Samuel and Romain from Uploaders
  * Bump debhelper compat level to 13

 -- Stéphane Glondu <glondu@debian.org>  Tue, 25 Aug 2020 08:07:19 +0200

ocaml-flac (0.1.6-1) unstable; urgency=medium

  * New upstream version 0.1.6
  * Move to dh build
  * Bump Standards-Version to 4.5.0 (Rules-Requires-Root, use dh)
  * Move d/copyright to dep-5 format
  * Bump compat to 12

 -- Kyle Robbertze <paddatrapper@debian.org>  Thu, 23 Apr 2020 13:02:00 +0200

ocaml-flac (0.1.5-2) unstable; urgency=medium

  * Rebuild for source-only upload

 -- Kyle Robbertze <paddatrapper@debian.org>  Sun, 14 Jul 2019 07:00:15 +0200

ocaml-flac (0.1.5-1) unstable; urgency=medium

  * New upstream version 0.1.5
  * Update ocaml-nox dependency to >=4.03
  * Update Standards-Version (no change)

 -- Kyle Robbertze <paddatrapper@debian.org>  Sun, 07 Jul 2019 10:41:40 +0200

ocaml-flac (0.1.3-2) unstable; urgency=medium

  * Fix VCS-Git URL
  * Install examples

 -- Kyle Robbertze <krobbertze@gmail.com>  Mon, 25 Jun 2018 13:46:16 +0200

ocaml-flac (0.1.3-1) unstable; urgency=medium

  * debian/watch: Upstream moved to GitHub
  * New upstream version 0.1.3
  * Bump standards version to 4.1.4 (no change)
  * Bump compat level to 10
  * Remove useless autoreconf build-depends
  * Update VCS-* to point to salsa

 -- Kyle Robbertze <krobbertze@gmail.com>  Wed, 13 Jun 2018 15:54:59 +0200

ocaml-flac (0.1.1-4) unstable; urgency=medium

  * Team upload.
  * Add build-dependency on dh-autoreconf to fix FTBFS on arm64. Thanks to
    Chen Baozi for the patch (closes: #778899).
  * Fix typo in short description of libflac-ocaml-dev
  * d/control: update Vcs-* fields
  * Standards-Version 3.0.8 (no change)

 -- Ralf Treinen <treinen@debian.org>  Sat, 27 Aug 2016 17:20:14 +0200

ocaml-flac (0.1.1-3) unstable; urgency=low

  * Upload to unstable.
  * Force build-dep on ocaml-ogg >= 0.4.5

 -- Romain Beauxis <toots@rastageeks.org>  Wed, 08 May 2013 19:03:10 -0500

ocaml-flac (0.1.1-2) experimental; urgency=low

  * Upload to experimental to rebuild against
    ocaml-ogg 0.4.4.
  * Bumped standards version to 3.9.4

 -- Romain Beauxis <toots@rastageeks.org>  Sat, 20 Apr 2013 11:16:03 -0500

ocaml-flac (0.1.1-1) unstable; urgency=low

  * New upstream release.

 -- Romain Beauxis <toots@rastageeks.org>  Tue, 26 Jun 2012 06:53:31 +0200

ocaml-flac (0.1.0-1) unstable; urgency=low

  * New upstream release.
  * Initial upload to sid.

 -- Romain Beauxis <toots@rastageeks.org>  Tue, 05 Jul 2011 12:13:30 -0500

ocaml-flac (0.1.0~20110532+hgb02e02b1cf7c-1) experimental; urgency=low

  * Initial upload to experimental (Closes: #627729)

 -- Romain Beauxis <toots@rastageeks.org>  Mon, 23 May 2011 17:22:12 -0500
